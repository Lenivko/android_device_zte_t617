#
# Copyright (C) 2015 The CyanogenMod Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Inherit from the common Open Source product configuration
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit from hardware-specific part of the product configuration
$(call inherit-product, device/zte/t617/device.mk)
$(call inherit-product-if-exists, vendor/zte/t617/t617-vendor.mk)

# Device identifier. This must come after all inclusions.
PRODUCT_DEVICE := t617
PRODUCT_NAME := full_t617
PRODUCT_BRAND := zte
PRODUCT_MODEL := ZTE T617
PRODUCT_MANUFACTURER := zte
